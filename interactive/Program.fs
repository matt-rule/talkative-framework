﻿open System
open Talkative

let rec mainLoop (parser : Parsing) =
    do Console.Write("> ")
    Console.WriteLine(
        match parser.parseSentenceFromString(Console.ReadLine()) with
            | SentenceParseError (input, err) -> "ERROR - " + err + " [\"" + input + "\"]"
            | ParsedSentence sentence -> "SUCCESS - " + sentence.ToString())
    mainLoop parser

[<EntryPoint>]
let main argv =
    let parser = Parsing(Test.TestData.dict)
    mainLoop parser
