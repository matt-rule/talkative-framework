﻿namespace Talkative.Test

open NUnit.Framework
open Talkative
open System

module public TestData =
    // this dict can contain duplicates if the wordType is different
    let dict =
        [
            { word = "a"; wordType = WordType.Determiner };
            { word = "an"; wordType = WordType.Determiner };
            { word = "and"; wordType = WordType.Conjunction };
            { word = "angry"; wordType = WordType.Adjective };
            { word = "are"; wordType = WordType.Verb };
            { word = "as"; wordType = WordType.Adverb };
            { word = "barked"; wordType = WordType.Verb };
            { word = "because"; wordType = WordType.Conjunction };
            { word = "big"; wordType = WordType.Adjective };
            { word = "break"; wordType = WordType.Verb };
            { word = "broke"; wordType = WordType.Verb };
            { word = "but"; wordType = WordType.Conjunction };
            { word = "did"; wordType = WordType.AuxVerb };
            { word = "does"; wordType = WordType.AuxVerb };
            { word = "dog"; wordType = WordType.CountableNoun };
            { word = "dropped"; wordType = WordType.Verb };
            { word = "drops"; wordType = WordType.Verb };
            { word = "end"; wordType = WordType.Verb };
            { word = "for"; wordType = WordType.Preposition };
            { word = "France"; wordType = WordType.Pronoun };
            { word = "Fred"; wordType = WordType.Pronoun };
            { word = "go"; wordType = WordType.Verb };
            { word = "good"; wordType = WordType.Adjective };
            { word = "Harry"; wordType = WordType.Pronoun };
            { word = "imperative"; wordType = WordType.Adjective };
            { word = "in"; wordType = WordType.Preposition };
            { word = "is"; wordType = WordType.AuxVerb };
            { word = "is"; wordType = WordType.Verb };
            { word = "it"; wordType = WordType.Pronoun };
            { word = "laughing"; wordType = WordType.Verb };
            { word = "letter"; wordType = WordType.CountableNoun };
            { word = "lowercase"; wordType = WordType.Adjective };
            { word = "me"; wordType = WordType.Pronoun };
            { word = "missing"; wordType = WordType.Verb };
            { word = "not"; wordType = WordType.Adverb };
            { word = "noun"; wordType = WordType.CountableNoun };
            { word = "Paris"; wordType = WordType.Pronoun };
            { word = "parse"; wordType = WordType.Verb };
            { word = "parser"; wordType = WordType.CountableNoun };
            { word = "period"; wordType = WordType.CountableNoun };
            { word = "praises"; wordType = WordType.Verb };
            { word = "rained"; wordType = WordType.Verb };
            { word = "sentence"; wordType = WordType.CountableNoun };
            { word = "she"; wordType = WordType.Pronoun };
            { word = "smash"; wordType = WordType.Verb };
            { word = "spoke"; wordType = WordType.Verb };
            { word = "sport"; wordType = WordType.CountableNoun };
            { word = "starting"; wordType = WordType.Verb };
            { word = "starts"; wordType = WordType.Verb };
            { word = "tennis"; wordType = WordType.Pronoun };
            { word = "the"; wordType = WordType.Determiner };
            { word = "this"; wordType = WordType.Determiner };
            { word = "to"; wordType = WordType.Preposition };
            { word = "tried"; wordType = WordType.Verb };
            { word = "vase"; wordType = WordType.CountableNoun };
            { word = "vegetables"; wordType = WordType.CountableNoun };
            { word = "was"; wordType = WordType.AuxVerb };
            { word = "was"; wordType = WordType.Verb };
            { word = "with"; wordType = WordType.Preposition };
            { word = "you"; wordType = WordType.Pronoun };
        ]

[<TestFixture>]
type TestUtil() =
    [<Test>]
    member this.TestSplitOut() =
        let testArray = [| 4; 16; 2; 9; 8 |]
        let (a, b) = Util.popIndex testArray 3
        do Assert.AreEqual([| 4; 16; 2 |], a)
        do Assert.AreEqual([| 8 |], b)

    [<Test>]
    member this.TestFindIndices() =
        let testArray = [| 4; 16; 2; 9; 8 |]
        let indices = Util.findIndices(fun x -> x > 8) testArray
        do Assert.AreEqual([| 1; 3 |], indices)

// See https://en.wikipedia.org/wiki/Predicate_(grammar)#In_traditional_grammar for predicate types
// The bool determines whether the test should classify this as a sentence.
[<TestFixture>]
type TestGrammar() =
    static member didAllPass (results : SentenceParseResult list) (answers : bool list) =
        let f (res : SentenceParseResult) =
            match res with
            | ParsedSentence _ -> true
            | SentenceParseError _ -> false
        let boolResults = List.map f results
        let zipped = List.zip answers boolResults
        let passed = List.map (fun (x, y) -> x = y) zipped
        List.forall (fun x -> x) passed

    member this.parseSentences sentences =
        let parser = Parsing(TestData.dict)
        let results = List.map (fun s -> parser.parseSentenceFromString (snd(s))) sentences
        let stringResults = results |> List.map (fun x -> x.ToString())
        //let convertResult (res : ClauseParseResult) =
        //    match res with
        //    | ParsedClause clause -> Some(clause)
        //    | ClauseParseError _ -> None
        //let hoverResults = List.map convertResult results
        TestGrammar.didAllPass results (List.map (fun s -> fst(s)) sentences)
        
    [<Test>]
    member this.TestParseBasicSentences() =
        let sentences =
            [
                (true, "It rained.");
                (true, "It rained");
                (true, "it rained.");
                (true, "It  rained.");
                (true, "It  rained .");
                (true, "It  rained. ");
                (true, " It  rained. ");
                (true, "IT RAINED.");
                (true, "It RaInEd.");
                (true, "Fred spoke.");
                (false, "Bzrkydgsnfg rained.");
                (false, "Bzrkydgsnfg dsdgdsgm.");
            ]
        Assert.IsTrue(this.parseSentences sentences)

    [<Test>]
    member this.TestParseDeterminers() =
        let sentences =
            [
                (true, "The dog barked.");
                (true, "A vase broke.");
                // We allow this one because we don't distinguish between nouns and pronouns yet.
                (true, "Vase broke")
                (false, "Vase a broke.");
                (false, "A the vase broke.");
                (false, "Vase broke a.");
                (false, "A broke vase.");
                (false, "A broke.");
            ]
        Assert.IsTrue(this.parseSentences sentences)

    [<Test>]
    member this.TestParseSubjVerbObj() =
        let sentences =
            [
                (true, "Fred is a dog.");
                // Predicative nominal over the subject - see https://en.wikipedia.org/wiki/Predicative_expression
                (true, "Tennis is a sport.");
                (true, "Fred praises Harry.");
                // Noun phrase, verb-plus-direct-object predicate
                (true, "Harry drops the vase.");
                (false, "Fred the vase drops.");
                (false, "Drops Fred the vase.");
                (false, "Not a sentence.");
            ]
        Assert.IsTrue(this.parseSentences sentences)

    [<Test>]
    member this.TestParseAdjectives() =
        let sentences =
            [
                (true, "The big dog barked.");
                (true, "The big angry dog barked.");
                (false, "The dog big barked.");
                (false, "The dog barked big.");
                (false, "Big the dog barked.");
            ]
        Assert.IsTrue(this.parseSentences sentences)

    [<Test>]
    member this.TestParseImperatives() =
        let sentences =
            [
                // Implied second-person subject, Verb-only predicate - see https://en.wikipedia.org/wiki/Imperative_mood
                (true, "Go.");
                //(true, "Don't go.");
            ]
        Assert.IsTrue(this.parseSentences sentences)
    
    [<Test>]
    member this.TestParseAuxVerbs() =
        let sentences =
            [
                (true, "Fred is laughing.");
                // Noun phrase, auxiliary verb, verb
                (true, "She was laughing.");
            ]
        Assert.IsTrue(this.parseSentences sentences)
    
    [<Test>]
    member this.TestParsePrepositions() =
        let sentences =
            [
                // "in France" - Predicative prepositional phrase
                // see https://en.wikipedia.org/wiki/Predicative_expression
                (true, "Paris is in France.");
                (true, "Fred is in France.");
                // Aux verb and preposition
                (true, "Fred is starting in France.");
                (true, "Fred is starting with France.");
                (true, "This sentence starts with a letter.");
                // ends with a prepositional phrase - "with a lowercase letter"
                // A prepositional phrase usually contains a noun phrase
                (true, "this sentence starts with a lowercase letter.");
                // ends with a prepositional phrase - "with a period"
                (true, "This sentence does not end with a period");
                (false, "Fred in is France.");
                (false, "Fred is in in France.");
            ]
        Assert.IsTrue(this.parseSentences sentences)
        
        let parser = Parsing(TestData.dict)
        let testSentence = "This sentence does not end with a period";
        let prepositionParseResult : SentenceParseResult = parser.parseSentenceFromString "This sentence does not end with a period"
        match prepositionParseResult with
        | SentenceParseError _ ->
            Assert.Fail("Could not parse test sentence: \"" + testSentence + "\"")
        | ParsedSentence sentence ->
            match sentence with
            | TwoClauses _ -> Assert.Fail("Not the expected sentence: \"" + testSentence + "\"")
            | SingleClause clause ->
                let pred = clause.predicate
                match pred.adverb with
                | None -> Assert.Fail("Could not parse test sentence: \"" + testSentence + "\"")
                | Some adv -> Assert.AreEqual("not", adv)

    [<Test>]
    member this.TestParseConjunctions() =
        let sentences =
            [
                (true, "The big dog barked and Fred dropped the vase.");
                (true, "Fred dropped the vase and the vase broke.");
                (true, "Fred dropped the vase but the vase did not break.");
                (false, "The big dog barked Fred dropped the vase.");
                (false, "and The big dog barked Fred dropped the vase.");
                (false, "The big dog barked Fred dropped the vase and.");
            ]
        Assert.IsTrue(this.parseSentences sentences)

    //[<Test>]
    //member this.ExtraTest() =
    //    raise(NotImplementedException())
    //    let sentences =
    //        [
    //            // There are two issues with parsing the below sentence:
    //            // 1. "tried to parse" - the parser doesn't understand the prepositional verb.
    //            // 2. "this sentence as an imperative sentence" - The parser is expecting only one noun here,
    //            //    so it incorrectly treats the second noun as part of the subject.
    //            (true, "The parser tried to parse this sentence as an imperative sentence because it was missing a noun.");
    //        ]
    //    Assert.IsTrue(this.parseSentences sentences)
