﻿namespace Talkative

// entity inherits data from archetype and then can override it with own properties
// noun is never a pronoun
// noun: noun used to describe this entity
// archetype: used for implicit features. If cats have ears, and Tom is a cat, Tom has ears unless stated otherwise.
//            this means we don't have to say cats have ears each time we specify a new one.
//            archetypes are going to be replaced with one or more "is-a" verb constructs
// adjectives: Give more information about this object to distinguish it from other objects sharing the same noun.
// subEntities: This is probably going to be a list of tagged unions. Each one can be either a NounPhrase or an Entity in its own right.
//              It's a NounPhrase if we don't need verbs attached to the object - "is a", "has a", "is X"
//              And the object doesn't have a location in its own right
//              An Entity is stored in this list if describing it would necessarily involve the holding object.
type Entity(noun : string, ?archetype : Archetype, ?adjectives : string list, ?subEntities : NounPhrase list) =
    member public this.noun = noun
    member public this.archetype = defaultArg (Some(archetype)) None
    member public this.adjectives = defaultArg adjectives []
    member public this.parts = defaultArg subEntities []
