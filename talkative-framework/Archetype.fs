﻿namespace Talkative

// An archetype cannot contain entities but it can contain nouns
type Archetype(mainNoun, ?verbPhrases, ?adjectives) =
    member public this.mainNoun : string = mainNoun
    member public this.verbPhrases : VerbPhrase list option = defaultArg (Some(verbPhrases)) None
    member public this.adjectives : string list option = defaultArg (Some(adjectives)) None