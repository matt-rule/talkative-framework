﻿namespace Talkative

type Event(subjectNoun : string, verb : string, objectNoun : string option) =
    member public this.subjectNoun = subjectNoun
    member public this.verb = verb
    member public this.objectNoun = objectNoun
    member public this.ToSentence() =
        Sentence.SingleClause
            (
            {
                subject = NounPhr(NounPhrase (subjectNoun))
                predicate =
                    Talkative.VerbPhrase
                        (
                        None, None, verb, None,
                        (
                            match objectNoun with
                            | None -> None
                            | Some value -> Some(NounPhrase(value))
                        )
                        )
            }
            )
    override this.ToString() =
        this.ToSentence().ToString()
