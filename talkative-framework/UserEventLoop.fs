﻿namespace Talkative

open System

module Main =
    let rec mainLoop (composer : Composer) sc =
        do Console.Write("> ")
        let input = Console.ReadLine()
        let nextSc, updateOutput = composer.getUpdateOutput(input, sc)
        do printfn "%A" updateOutput
        mainLoop composer nextSc