﻿namespace Talkative

open System

type public DeterminerParseResult =
    | ParsedDeterminer of determiner : string option
    | DeterminerParseError of error : string * string

type public AdjectiveParseResult =
    | ParsedAdjectives of adjectives : string list
    | AdjectiveParseError of error : string * string

type public BeforeNounParseResult =
    | ParsedBeforeNoun of determiner : string option * adjectives : string list
    | BeforeNounParseError of error : string * string

// The subject of a clause seems to correspond with a noun phrase
// The determiner of a noun phrase can be a quantifier or numeral.
// Initial constructor allows us to pass in a string option if we need to
type public NounPhrase(noun : string, ?determiner : string option, ?adjectives : string list) =
    new (noun : string, determiner : string, ?adjectives : string list) =
        NounPhrase(noun, Some(determiner), defaultArg adjectives [])
    new (noun : string, adjectives : string list) =
        NounPhrase(noun, None, adjectives)
    member public this.noun = noun
    member public this.determiner = defaultArg determiner None
    member public this.adjectives = defaultArg adjectives []
    override this.ToString() =
        [(
            match this.determiner with
            | None -> ""
            | Some d -> d
        )]
        @ this.adjectives
        @ [this.noun]
        |> List.where (fun x -> not (String.IsNullOrWhiteSpace x))
        |> String.concat " "

// In the ParseError the first string is the input, the second is the error message.
type public NounPhraseParseResult =
    | ParsedNounPhrase of np : NounPhrase
    | NounPhraseParseError of error : string * string
    | NounPhraseEmpty

type public Subject =
    | ImpliedAddressee
    | NounPhr of phrase : NounPhrase
    override this.ToString() =
        match this with
        | ImpliedAddressee -> ""
        | NounPhr phr -> phr.ToString()
    
type public SubjectParseResult =
    | ParsedSubject of subj : Subject
    | SubjectParseError of error : string * string

// A preposition is an example of an adposition.
type public AdpositionalPhrase = { adposition : string; np : NounPhrase } with
    override this.ToString() =
        this.adposition + " " + this.np.ToString()

type public AdpositionalPhraseParseResult =
    | ParsedAdpositionalPhrase of ap : AdpositionalPhrase
    | AdpositionalPhraseParseError of error : string * string
    
// Auxiliary verb followed by adverb, both optional.
type public BeforeVerbParseResult =
    | ParsedBeforeVerb of successTuple : string option * string option
    | BeforeVerbParseError of error : string * string

type public AfterVerbParseResult =
    | ParsedAdpositionalPhrAfterVerb of adpPhrase : AdpositionalPhrase
    | ParsedObjectAfterVerb of obj : NounPhrase
    | EmptyAfterVerb
    | AfterVerbParseError of error : string * string

// In a clause, a predicate is typically a verb phrase (https://en.wikipedia.org/wiki/Clause)
type public VerbPhrase(auxVerb : string option, adverb : string option, mainVerb : string, preposition : AdpositionalPhrase option, object : NounPhrase option) =
    member public this.auxVerb = auxVerb
    member public this.adverb = adverb
    member public this.mainVerb = mainVerb
    member public this.preposition = preposition
    member public this.object = object
    override this.ToString() =
        [
            (
                match auxVerb with
                | None -> ""
                | Some v -> v
            );
            (
                match adverb with
                | None -> ""
                | Some v -> v
            );
            (mainVerb);
            (
                match preposition with
                | None -> ""
                | Some p -> p.ToString()
            );
            (
                match object with
                | None -> ""
                | Some o -> o.ToString()
            );
        ]
        |> List.where (fun x -> not (String.IsNullOrWhiteSpace x))
        |> String.concat " "


// In the ParseError the first string is the input, the second is the error message.
type public VerbPhraseParseResult =
    | ParsedVerbPhrase of vp : VerbPhrase
    | VerbPhraseParseError of error : string * string

type public Clause = { subject : Subject; predicate : VerbPhrase} with
    override this.ToString() =
        this.subject.ToString() + " " + this.predicate.ToString()

// In the ParseError the first string is the input, the second is the error message.
type public ClauseParseResult =
    | ParsedClause of clause : Clause
    | ClauseParseError of error : string * string

type public Sentence =
    | SingleClause of clause : Clause
    | TwoClauses of conjClauseClause : string * Clause * Clause
    override this.ToString() =
        let unpunctuated =
            match this with
            | SingleClause c -> c.ToString()
            | TwoClauses (conj, c1, c2) ->
                [
                    c1.ToString();
                    conj;
                    c2.ToString()
                ]
                |> List.where (fun x -> not (String.IsNullOrWhiteSpace x))
                |> String.concat " "
        match unpunctuated.Length with
        | 0 -> ""
        | _ ->
            (unpunctuated |> Seq.head |> Char.ToUpper |> string)
            + (unpunctuated |> Seq.tail |> String.Concat)
            + "."

type public SentenceParseResult =
    | ParsedSentence of sentence : Sentence
    | SentenceParseError of error : string * string
    override this.ToString() =
        match this with
        | ParsedSentence s -> s.ToString()
        | SentenceParseError (inp, err) -> "SentenceParseError (" + inp + ", " + err + ")"

// Verb, noun...
type public WordType =
    | CountableNoun
    | MassNoun
    | Verb
    | AuxVerb
    | Pronoun
    | Adjective
    | Adverb
    | Determiner
    | Preposition
    | Conjunction

// A Token list is a pattern matching a set of sentences and is used in parsing.
type public Token =
    // Exact string match (should not include spaces).
    | Text of string
    // Any word of the given WordType.
    | KnownType of WordType
    // Does not include the first word of a sentence. Used to identify names.
    | Capital
    // Any alphabetic text (no spaces or special characters).
    | Wildcard

// Make a list of these to store known words along with their WordTypes.
type public DictionaryEntry = { word : string; wordType : WordType }

type public Parsing (i_dict : DictionaryEntry list) =

    member this.dict = i_dict

    member this.isDictionaryMatch (word : string) dict =
        not (List.isEmpty (List.where(fun entry -> entry.word.ToLower() = word.ToLower()) dict))

    member this.allOfType (dict : DictionaryEntry list) (wordTypes : WordType list) =
        List.where (fun (dictEntry : DictionaryEntry) -> (List.exists (fun wordType -> dictEntry.wordType = wordType) wordTypes)) dict
        
    member this.isMatchOfType (wordType : WordType list) word =
        this.isDictionaryMatch word (this.allOfType this.dict wordType)
    
    member this.parseDeterminer (wordsToParse : string []) =
        match wordsToParse.Length with
        | 0 -> ParsedDeterminer(None)
        | 1 ->
            let word = wordsToParse.[0]
            if this.isMatchOfType [WordType.Determiner] word
            then ParsedDeterminer(Some(word))
            else DeterminerParseError(Util.concatWords wordsToParse, "Could not parse determiner.")
        | _ -> DeterminerParseError(Util.concatWords wordsToParse, "Could not parse determiner - expected one word only.")

    member this.parseAdjectives (wordsToParse : string []) =
        if Array.forall (this.isMatchOfType [WordType.Adjective]) wordsToParse
        then ParsedAdjectives(List.ofArray wordsToParse)
        else AdjectiveParseError(Util.concatWords wordsToParse, "Could not parse all words as adjectives")

    member this.parseBeforeNoun (wordsToParse : string []) : BeforeNounParseResult =
        let determinerWordList = Array.takeWhile (this.isMatchOfType [WordType.Determiner]) wordsToParse
        match this.parseDeterminer determinerWordList with
        | DeterminerParseError (inp, err) -> BeforeNounParseError(inp, err)
        | ParsedDeterminer determiner ->
            let adjectiveWordList = Array.skipWhile (this.isMatchOfType [WordType.Determiner]) wordsToParse
            match this.parseAdjectives adjectiveWordList with
            | AdjectiveParseError (inp, err) -> BeforeNounParseError(inp, err)
            | ParsedAdjectives adjs -> ParsedBeforeNoun(determiner, adjs)

    member this.parseNounPhrase (wordsToParse : string []) : NounPhraseParseResult =
        if (Array.isEmpty wordsToParse)
        then
            NounPhraseEmpty
        else
            if not(Array.exists (this.isMatchOfType [WordType.CountableNoun; WordType.MassNoun; WordType.Pronoun]) wordsToParse)
            then NounPhraseParseError((Util.concatWords wordsToParse, "No noun found when parsing noun phrase."))
            else
                let nounIndex : int = Array.findIndex (this.isMatchOfType [WordType.CountableNoun; WordType.MassNoun; WordType.Pronoun]) wordsToParse
                let (noun, beforeNoun, afterNoun) = Util.splitOutIndex wordsToParse nounIndex
                if not (Array.isEmpty afterNoun)
                then NounPhraseParseError(Util.concatWords wordsToParse, "Did not expect anything after noun when parsing noun phrase.")
                else
                    match this.parseBeforeNoun beforeNoun with
                    | BeforeNounParseError (inp, err) -> NounPhraseParseError (inp, err)
                    | ParsedBeforeNoun (determiner, adjectives) -> ParsedNounPhrase (NounPhrase(noun, determiner, adjectives))
    
    member this.parsePreposPhrase (wordsToParse : string []) : AdpositionalPhraseParseResult =
        let preposIndex = Array.tryFindIndex (this.isMatchOfType [WordType.Preposition]) wordsToParse
        match preposIndex with
        | None -> AdpositionalPhraseParseError (Util.concatWords wordsToParse, "Could not parse adpositional phrase.")
        | Some validIndex ->
            let (preposition, beforePrepos, afterPrepos) = Util.splitOutIndex (wordsToParse) validIndex

            if not (Array.isEmpty beforePrepos)
            then
                AdpositionalPhraseParseError (Util.concatWords wordsToParse, "Could not parse adpositional phrase.")
            else
                match this.parseNounPhrase afterPrepos with
                | NounPhraseParseError (inp, err) -> AdpositionalPhraseParseError (inp, err)
                | ParsedNounPhrase parsedNP -> ParsedAdpositionalPhrase({ adposition = preposition; np = parsedNP })
                | NounPhraseEmpty -> AdpositionalPhraseParseError (Util.concatWords wordsToParse, "Could not find noun phrase inside adpositional phrase.")
    
    // Returns auxiliary verb followed by adverb if they exist.
    member this.parseBeforeVerb (wordsToParse : string []) : BeforeVerbParseResult =
        let auxVerbIndex = Array.tryFindIndex (this.isMatchOfType [WordType.AuxVerb]) wordsToParse
        match auxVerbIndex with
        | None ->
            if (wordsToParse.Length <> 0)
            then BeforeVerbParseError (Util.concatWords wordsToParse, "Could not parse before verb in verb phrase.")
            else ParsedBeforeVerb(None, None)
        | Some auxIndex ->
            let (auxVerb, beforeAuxVerb, afterAuxVerb) = Util.splitOutIndex wordsToParse auxIndex
            if (beforeAuxVerb.Length <> 0)
            then BeforeVerbParseError (Util.concatWords wordsToParse, "Could not parse before verb in verb phrase.")
            else
                let adverbIndex = Array.tryFindIndex (this.isMatchOfType [WordType.Adverb]) afterAuxVerb
                match adverbIndex with
                | None -> ParsedBeforeVerb(Some(auxVerb), None)
                | Some advIndex ->
                    let (adverb, beforeAdverb, afterAdverb) = Util.splitOutIndex afterAuxVerb advIndex
                    if (beforeAdverb.Length <> 0 || afterAdverb.Length <> 0)
                    then BeforeVerbParseError (Util.concatWords wordsToParse, "Could not parse before verb in verb phrase.")
                    else
                        ParsedBeforeVerb(Some(auxVerb), Some(adverb))

    member this.parseAfterVerb (wordsToParse : string []) : AfterVerbParseResult =
        // Could be a noun phrase or a prepositional phrase containing one.
        // Try to parse as a prepositional phrase, if that fails then try the other one.
        match this.parsePreposPhrase wordsToParse with
        | ParsedAdpositionalPhrase ap -> ParsedAdpositionalPhrAfterVerb(ap)
        | AdpositionalPhraseParseError _ ->
            match this.parseNounPhrase wordsToParse with
            | ParsedNounPhrase obj -> ParsedObjectAfterVerb(obj)
            | NounPhraseParseError (inp, err) -> AfterVerbParseError (inp, err)
            | NounPhraseEmpty -> EmptyAfterVerb

    member this.parseVerbPhrase (wordsToParse : string []) : VerbPhraseParseResult =
        // Pick the last verb in wordsToParse if there is one.
        let verbIndex = Array.tryFindIndexBack (this.isMatchOfType [WordType.Verb]) wordsToParse
        match verbIndex with
        | None -> VerbPhraseParseError((Util.concatWords wordsToParse, "No verb found when parsing verb phrase."))
        | Some index ->
            let (verb, beforeVerb, afterVerb) = Util.splitOutIndex(wordsToParse) index
            match this.parseBeforeVerb beforeVerb with
            | BeforeVerbParseError (inp, err) -> VerbPhraseParseError (inp, err)
            | ParsedBeforeVerb (auxVerb, adverb) ->
                match this.parseAfterVerb afterVerb with
                | AfterVerbParseError (inp, err) -> VerbPhraseParseError (inp, err)
                | EmptyAfterVerb -> ParsedVerbPhrase(VerbPhrase(auxVerb, adverb, verb, None, None))
                | ParsedAdpositionalPhrAfterVerb ap -> ParsedVerbPhrase(VerbPhrase(auxVerb, adverb, verb, Some(ap), None))
                | ParsedObjectAfterVerb obj -> ParsedVerbPhrase(VerbPhrase(auxVerb, adverb, verb, None, Some(obj)))
                
    // This function finds out whether the subject is missing.
    member this.parseSubject (wordsToParse : string []) : SubjectParseResult =
        if Array.isEmpty wordsToParse
        then
            ParsedSubject(ImpliedAddressee)
        else
            if not (Array.exists (this.isMatchOfType [WordType.CountableNoun; WordType.MassNoun; WordType.Pronoun]) wordsToParse)
            then
                SubjectParseError(Util.concatWords wordsToParse, "Could not parse subject: Found words but they were not nouns.")
            else
                match this.parseNounPhrase wordsToParse with
                | NounPhraseParseError (inp, err) -> SubjectParseError(inp, err)
                | ParsedNounPhrase phr -> ParsedSubject(NounPhr(phr))
                | NounPhraseEmpty -> SubjectParseError(Util.concatWords wordsToParse, "Error while parsing subject. Noun phrase empty.")
    
    //  Splitting at the noun is preferable to splitting at the verb,
    //  because the division between noun phrase and verb phrase happens next to the first noun in all examples so far considered.
    member this.parseClause (wordsToParse : string []) : ClauseParseResult =
        let nounIndex = Array.tryFindIndex (this.isMatchOfType [WordType.CountableNoun; WordType.MassNoun; WordType.Pronoun]) wordsToParse
        match nounIndex with
        | None ->
            match this.parseVerbPhrase wordsToParse with
            | VerbPhraseParseError (inp, err) -> ClauseParseError (inp, "Tried to parse sentence as an imperative sentence because it was missing a noun: " + err)
            | ParsedVerbPhrase pred -> ParsedClause({subject = ImpliedAddressee; predicate = pred})
        | Some index ->
            let (noun, beforeNoun, afterNoun) = Util.splitOutIndex(wordsToParse) index
            match this.parseSubject (Array.concat ([beforeNoun; [|noun|]])) with
            | SubjectParseError (inp, err) -> ClauseParseError (inp, err)
            | ParsedSubject subj ->
                match this.parseVerbPhrase afterNoun with
                | VerbPhraseParseError (inp, err) -> ClauseParseError (inp, err)
                | ParsedVerbPhrase pred -> ParsedClause({subject = subj; predicate = pred})
    
    member this.parseSentence (wordsToParse : string []) =
        let conjunctionIndex = Array.tryFindIndex (this.isMatchOfType [WordType.Conjunction]) wordsToParse
        match conjunctionIndex with
        | None ->
            match this.parseClause wordsToParse with
            | ClauseParseError (inp, err) -> SentenceParseError (inp, err)
            | ParsedClause clause -> ParsedSentence(SingleClause clause)
        | Some index ->
            let (conjunction, beforeConj, afterConj) = Util.splitOutIndex(wordsToParse) index
            match this.parseClause beforeConj with
            | ClauseParseError (inp, err) -> SentenceParseError (inp, err)
            | ParsedClause firstClause ->
                match this.parseClause afterConj with
                | ClauseParseError (inp, err) -> SentenceParseError (inp, err)
                | ParsedClause sndClause ->
                    ParsedSentence(TwoClauses(conjunction, firstClause, sndClause))

    member this.parseSentenceFromString (s : string) : SentenceParseResult =
        let sentenceArray =
            s.Split('.', StringSplitOptions.RemoveEmptyEntries)
            |> Array.where (fun x -> not (String.IsNullOrWhiteSpace x)) 
        if Array.length sentenceArray > 1
        then
            SentenceParseError(s, "Not a single sentence.")
        else
            // Split after the first noun and pass into respective functions.
            let wordArray = sentenceArray.[0].Split(' ', StringSplitOptions.RemoveEmptyEntries)
            this.parseSentence wordArray
            