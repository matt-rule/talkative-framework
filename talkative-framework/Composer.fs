﻿namespace Talkative

open System

// all entities in this bucket have the same features
type EntityWithDeterminer(entity : Entity, determiner : string) =
    member public this.entity = entity
    member public this.determiner = determiner

type Composer (i_dict : DictionaryEntry list, archetypes) =

    member public this.dict = i_dict
    member public this.parser = Parsing(i_dict)

    // the string option is for defining a special plural
    member public this.archetypeList : Entity list = archetypes
    member public this.knownArchetypeList = this.archetypeList

    member public this.getArchetype noun =
        this.archetypeList |> List.tryFind (fun x -> x.noun = noun)
        
    member public this.beginsWithVowel s =
        if String.IsNullOrEmpty s
        then false
        else
            if ['a'; 'e'; 'i'; 'o'; 'u'] |> List.contains (s.Chars(0))
            then true
            else false
            
    member public this.getAOrAn s =
        if this.beginsWithVowel s then "an" else "a"

    member public this.getAOrAnList l =
        if l |> List.isEmpty
        then "a"
        else this.getAOrAn (l |> List.head)
    
    // this should be done at the end of forming the sentence
    member public this.delimit (delimiter : string) (l : string list) : string list =
        if l |> List.isEmpty
        then []
        else [l |> List.fold(fun x y -> x + (if x = "" then "" else delimiter) + y) ""]

    member public this.describeListNoAnd (l : string list) : string list =
        l |> this.delimit ", "

    member public this.allButLast l : 'a list =
        seq {for i in 0..(l |> List.length)-1 do if i <> (l |> List.length) - 1 then yield (l.Item i)} |> Seq.toList

    member public this.describeList (l : string list) : string list =
        if l.Length = 0
        then
            []
        else if l.Length = 1
        then
            [l |> List.exactlyOne]
        else
            (
                l
                |> this.allButLast
                |> this.delimit ", "
            )
            @ ["and"]
            @ [l |> List.last]

    member public this.describeEntityBucket (extraAdjectives : string list, ed : EntityWithDeterminer) : string =
        let (adjectives : string list) = ((ed.entity.adjectives @ extraAdjectives) |> this.describeListNoAnd |> this.delimit " ")
        let subEntities =
            this.describeList
                (
                    (ed.entity.parts)
                    |> List.map
                        (
                        fun (np : NounPhrase) ->
                        (
                        this.describeEntityBucket
                            (
                            np.adjectives,
                            (
                            EntityWithDeterminer
                                (
                                (this.getArchetype np.noun).Value,
                                match np.determiner with
                                    | None -> "a"
                                    | Some det -> det
                                )
                            )
                            )
                        )
                        )
                )
        
        let phrase =
            adjectives
            @
            [
                ed.entity.noun
                +
                if (this.parser.isMatchOfType([WordType.MassNoun]) ed.entity.noun || ed.determiner = "one")
                then ""
                else "s"
            ]

        (
            this.delimit " "
                (
                    (
                        if (this.parser.isMatchOfType([WordType.MassNoun]) ed.entity.noun || ed.determiner <> "one")
                        then []
                        else [this.getAOrAnList phrase]
                    )
                    @ phrase
                    @
                    if subEntities = []
                    then []
                    else
                        (
                            (
                                if ed.determiner <> "one"
                                then ["each"]
                                else []
                            )
                            @ ["with"]
                            @ subEntities
                        )
                )
            |> List.head
        )
    
    member public this.getDescriptionEntity (e : Entity) =
        this.describeEntityBucket ([], EntityWithDeterminer(e, "one"))
    
    member public this.getDescriptionEvent (e : Event) =
        this.describeEvent e
    
    member public this.getUpdateOutput (input : String, scene : Scene) : (Scene * String) =
        let nextState, eventList = input |> (scene |> scene.updateFunc)
        let eventDescriptions = eventList |> List.map(fun e -> this.getDescriptionEvent e)
        let updateOutput = eventDescriptions |> this.delimit("\n")
        (
            nextState,
            if (updateOutput.IsEmpty) then "" else updateOutput.Head
        )

    member public this.describeEvent (event : Event) =
        event.ToString()