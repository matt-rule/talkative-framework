﻿namespace Talkative

type Scene(entityList, events, timePassed, updateFunc) =

    member public this.entityList : Entity list = entityList
    member public this.events : Event list = events
    member public this.timePassed : int = timePassed
    member public this.updateFunc : Scene -> string -> (Scene * Event list) = updateFunc
