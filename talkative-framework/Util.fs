﻿namespace Talkative

module public Util =
    let allMatch tupleList =
        not (List.exists (fun i -> fst(i) <> (snd(i))) tupleList)
    
    // index must be < words.Length
    let popIndex (words : 'a []) (index : int) =
        let beforeIndex, unSplit = Array.splitAt index words
        let _, afterIndex = Array.splitAt 1 unSplit
        (beforeIndex, afterIndex)

    let splitOutIndex (words : 'a []) (index : int) =
        let (beforeIndex, afterIndex) = popIndex words index
        (words.[index], beforeIndex, afterIndex)

    let concatWords wordArray =
        Array.fold (fun x y -> x + (if x = "" then "" else " ") + y) "" wordArray

    let findIndices (predicate : 'a -> bool) (array : 'a []) : int [] =
        let tuplePredicate (tuple : (int * 'a)) : bool =
            predicate (snd(tuple))
        let indexed = Array.indexed array
        let filtered = Array.where(tuplePredicate) indexed
        Array.map (fun tuple -> fst(tuple)) filtered
